<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '43b56d876e68645636f6573008c5e9a41be8ddcdca08bbc1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0eq<pqF3`)| 6;Mbb1qr8Q:4y~TaS~?<WsL>yFg&Si(oBEzg9U,)^x@ZE(vx%2KB');
define('SECURE_AUTH_KEY',  ']:KVhXH_m}mdWcoBFQcS<?zz?:Z4{)<l;FDiY=wazjNrdh^<N[O4r=e|O,_]-#9;');
define('LOGGED_IN_KEY',    ':IvKyi!?Km%|5TGi~|3H$rF;r2WVT`VXHZjQNkLOag }gXq=w-z%zk[a0bGoKl`]');
define('NONCE_KEY',        'yDY&>j%j<_Lhs4L4.Ou|8R=#g[J^h4iqaBgq0GK<P?Mf%O0u[&Hzcif{6|Oi?X<7');
define('AUTH_SALT',        '.JAo!:&S,qbz`$g94xxK,3ir.;KA<]!8NMN}6zD5rJ=cQ.A=o[T6EstilcBaK;X2');
define('SECURE_AUTH_SALT', 't9V@NDzQ[D*(ut2B}]FU:;.G`n+Skf[m[VpO5?v{)kGn#x@)x3x)Ene+^=Y/{4[2');
define('LOGGED_IN_SALT',   'g#wY,.}iPyW-GWvCb1~PILU3W^H %(} |TcLdK;0?vjr4_v(cpi>N9_>TA$Lcq0u');
define('NONCE_SALT',       '=uS/Sh{*Jo cX6DVdM<~+Od4[$SVf>1+}~BQpJE4 J${-uyO@crrs(T+nW=8^jJg');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
