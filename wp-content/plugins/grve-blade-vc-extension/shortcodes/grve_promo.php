<?php
/**
 * Single Expandable Info Shortcode
 */

if( !function_exists( 'grve_blade_vce_promo_shortcode' ) ) {

	function grve_blade_vce_promo_shortcode( $atts, $content ) {

		$output = $button = $retina_data = $el_class = '';

		extract(
			shortcode_atts(
				array(
					'image' => '',
					'retina_image' => '',
					'align' => 'left',
					'button_text' => '',
					'button_link' => '',
					'button_type' => 'simple',
					'button_size' => 'medium',
					'button_color' => 'primary-1',
					'button_hover_color' => 'black',
					'button_shape' => 'square',
					'button_class' => '',
					'btn_add_icon' => '',
					'btn_icon_library' => 'fontawesome',
					'btn_icon_fontawesome' => 'fa fa-adjust',
					'btn_icon_openiconic' => '',
					'btn_icon_typicons' => '',
					'btn_icon_entypo' => '',
					'btn_icon_linecons' => '',
					'btn_icon_simplelineicons' => '',
					'margin_bottom' => '',
					'el_class' => '',
				),
				$atts
			)
		);

		//Button
		$button_options = array(
			'button_text'  => $button_text,
			'button_link'  => $button_link,
			'button_type'  => $button_type,
			'button_size'  => $button_size,
			'button_color' => $button_color,
			'button_hover_color' => $button_hover_color,
			'button_shape' => $button_shape,
			'button_class' => $button_class,
			'btn_add_icon' => $btn_add_icon,
			'btn_icon_library' => $btn_icon_library,
			'btn_icon_fontawesome' => $btn_icon_fontawesome,
			'btn_icon_openiconic' => $btn_icon_openiconic,
			'btn_icon_typicons' => $btn_icon_typicons,
			'btn_icon_entypo' => $btn_icon_entypo,
			'btn_icon_linecons' => $btn_icon_linecons,
			'btn_icon_simplelineicons' => $btn_icon_simplelineicons,
		);
		$button = grve_blade_vce_get_button( $button_options );
		$image_string = '';

		if ( !empty( $image ) ) {

			$id = preg_replace('/[^\d]/', '', $image);
			$thumb_src = wp_get_attachment_image_src( $id, 'full' );
			$image_dimensions = 'width="' . $thumb_src[1] . '" height="' . $thumb_src[2] . '"';
			if ( !empty( $retina_image ) ) {
				$img_retina_id = preg_replace('/[^\d]/', '', $retina_image);
				$img_retina_src = wp_get_attachment_image_src( $img_retina_id, 'full' );
				$retina_data = ' data-at2x="' . esc_attr( $img_retina_src[0] ) . '"';
			}
			$alt = get_post_meta( $id, '_wp_attachment_image_alt', true );

			$image_string .= '<img class="grve-expandable-info-logo" src="' . esc_url( $thumb_src[0] ) . '" alt="' . esc_attr( $alt ) . '" ' . $image_dimensions . $retina_data . '>';
		}

		$output .= '<div class="grve-element grve-expandable-info grve-align-' . $align . ' ' . esc_attr( $el_class ) . '">';
		$output .= $image_string;
		$output .= '  <div class="grve-expandable-info-content">';
		$output .= '  	<div class="grve-expandable-info-space"></div>';
		$output .= '    <p class="grve-leader-text">' . grve_blade_vce_unautop( $content ) . '</p>';
		$output .= $button;
		$output .= '  </div>';
		$output .= '</div>';

		return $output;
	}
	add_shortcode( 'grve_promo', 'grve_blade_vce_promo_shortcode' );

}

/**
 * Add shortcode to Visual Composer
 */

if( !function_exists( 'grve_blade_vce_promo_shortcode_params' ) ) {
	function grve_blade_vce_promo_shortcode_params( $tag ) {

		$grve_blade_vce_promo_shortcode_btn_params = grve_blade_vce_get_button_params();
		$grve_blade_vce_promo_shortcode_params = array_merge(
			array(
				array(
					"type" => "attach_image",
					"heading" => esc_html__( "Image", "grve-blade-vc-extension" ),
					"param_name" => "image",
					"value" => '',
					"description" => esc_html__( "Select an image.", "grve-blade-vc-extension" ),
				),
				array(
					"type" => "attach_image",
					"heading" => esc_html__( "Retina Image", "grve-blade-vc-extension" ),
					"param_name" => "retina_image",
					"value" => '',
					"description" => esc_html__( "Select a 2x image.", "grve-blade-vc-extension" ),
				),
				array(
					"type" => "textarea",
					"heading" => esc_html__( "Text", "grve-blade-vc-extension" ),
					"param_name" => "content",
					"value" => "Sample Text",
					"description" => esc_html__( "Enter your text.", "grve-blade-vc-extension" ),
				),
				grve_blade_vce_add_align(),
				grve_blade_vce_add_el_class(),
			),
			$grve_blade_vce_promo_shortcode_btn_params
		);

		return array(
			"name" => esc_html__( "Advanced Promo", "grve-blade-vc-extension" ),
			"description" => esc_html__( "Advanced, impressive promotion for whatever you like", "grve-blade-vc-extension" ),
			"base" => $tag,
			"class" => "",
			"icon"      => "icon-wpb-grve-promo",
			"category" => esc_html__( "Content", "js_composer" ),
			"params" => $grve_blade_vce_promo_shortcode_params,
		);
	}
}

if( function_exists( 'vc_lean_map' ) ) {
	vc_lean_map( 'grve_promo', 'grve_blade_vce_promo_shortcode_params' );
} else if( function_exists( 'vc_map' ) ) {
	$attributes = grve_blade_vce_promo_shortcode_params( 'grve_promo' );
	vc_map( $attributes );
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
