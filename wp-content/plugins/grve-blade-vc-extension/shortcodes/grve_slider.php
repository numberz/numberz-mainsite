<?php
/**
 * Slider Shortcode
 */

if( !function_exists( 'grve_blade_vce_slider_shortcode' ) ) {

	function grve_blade_vce_slider_shortcode( $attr, $content ) {

		$output = $class_fullwidth = $el_class = '';

		extract(
			shortcode_atts(
				array(
					'ids' => '',
					'image_mode' => 'resize',
					'slideshow_speed' => '3500',
					'navigation_type' => '1',
					'navigation_color' => 'dark',
					'pause_hover' => 'no',
					'auto_play' => 'yes',
					'auto_height' => 'no',
					'margin_bottom' => '',
					'el_class' => '',
				),
				$attr
			)
		);

		$attachments = explode( ",", $ids );

		if ( empty( $attachments ) ) {
			return '';
		}

		$image_size = 'blade-grve-fullscreen';
		if ( 'autocrop' == $image_mode ) {
			$image_size = 'blade-grve-large-rect-horizontal';
		}

		$style = grve_blade_vce_build_margin_bottom_style( $margin_bottom );

		$slider_data = '';
		$slider_data .= ' data-slider-speed="' . esc_attr( $slideshow_speed ) . '"';
		$slider_data .= ' data-slider-pause="' . esc_attr( $pause_hover ) . '"';
		$slider_data .= ' data-slider-autoplay="' . esc_attr( $auto_play ) . '"';
		$slider_data .= ' data-slider-autoheight="' . esc_attr( $auto_height ) . '"';

		$output .= '<div class="grve-element grve-carousel-wrapper"  style="' . $style . ' ">';

		//Slider Navigation
		$output .= grve_blade_vce_element_navigation( $navigation_type, $navigation_color );

		$output .= '<div class="grve-slider grve-carousel-element ' . esc_attr( $el_class ) . '"' . $slider_data . '>';

		foreach ( $attachments as $id ) {
			$image_link_href =  wp_get_attachment_url( $id );
			$thumb_src = wp_get_attachment_image_src( $id, $image_size );
			$image_dimensions = 'width="' . $thumb_src[1] . '" height="' . $thumb_src[2] . '"';

			$image_title = get_post_field( 'post_title', $id );
			$caption = get_post_field( 'post_excerpt', $id );
			$alt = get_post_meta( $id, '_wp_attachment_image_alt', true );
			$alt = ! empty( $alt ) ? $alt : wptexturize( $image_title );

			$output .= '<div class="grve-slider-item">';
			$output .= '	<figure>';
			$output .= '		<div class="grve-media">';
			$output .= '			<img src="' . esc_url( $thumb_src[0] ) . '" alt="' . esc_attr( $alt ) . '" ' . $image_dimensions . '>';
			$output .= '		</div>';
			$output .= '	</figure>';
			$output .= '</div>';

		}
		$output .= '	</div>';
		$output .= '</div>';

		return $output;

	}
	add_shortcode( 'grve_slider', 'grve_blade_vce_slider_shortcode' );

}

/**
 * Add shortcode to Visual Composer
 */

if( !function_exists( 'grve_blade_vce_slider_shortcode_params' ) ) {
	function grve_blade_vce_slider_shortcode_params( $tag ) {
		return array(
			"name" => esc_html__( "Slider", "grve-blade-vc-extension" ),
			"description" => esc_html__( "Create a simple slider", "grve-blade-vc-extension" ),
			"base" => $tag,
			"class" => "",
			"icon"      => "icon-wpb-grve-slider",
			"category" => esc_html__( "Content", "js_composer" ),
			"params" => array(
				array(
					"type"			=> "attach_images",
					"admin_label"	=> true,
					"class"			=> "",
					"heading"		=> esc_html__( "Attach Images", "grve-blade-vc-extension" ),
					"param_name"	=> "ids",
					"value" => '',
					"description"	=> esc_html__( "Select your slider images.", "grve-blade-vc-extension" ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( "Image Mode", "grve-blade-vc-extension" ),
					"param_name" => "image_mode",
					'value' => array(
						esc_html__( 'Resize', 'grve-blade-vc-extension' ) => 'resize',
						esc_html__( 'Auto Crop', 'grve-blade-vc-extension' ) => 'autocrop',
					),
					"description" => esc_html__( "Select your slider image mode.", "grve-blade-vc-extension" ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( "Autoplay", "grve-blade-vc-extension" ),
					"param_name" => "auto_play",
					"value" => array(
						esc_html__( "Yes", "grve-blade-vc-extension" ) => 'yes',
						esc_html__( "No", "grve-blade-vc-extension" ) => 'no',
					),
				),
				grve_blade_vce_add_slideshow_speed(),
				array(
					"type" => 'checkbox',
					"heading" => esc_html__( "Pause on Hover", "grve-blade-vc-extension" ),
					"param_name" => "pause_hover",
					"value" => array( esc_html__( "If selected, slider will be paused on hover", "grve-blade-vc-extension" ) => 'yes' ),
				),
				grve_blade_vce_add_auto_height(),
				grve_blade_vce_add_navigation_type(),
				grve_blade_vce_add_navigation_color(),
				grve_blade_vce_add_margin_bottom(),
				grve_blade_vce_add_el_class(),
			),
		);
	}
}

if( function_exists( 'vc_lean_map' ) ) {
	vc_lean_map( 'grve_slider', 'grve_blade_vce_slider_shortcode_params' );
} else if( function_exists( 'vc_map' ) ) {
	$attributes = grve_blade_vce_slider_shortcode_params( 'grve_slider' );
	vc_map( $attributes );
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
